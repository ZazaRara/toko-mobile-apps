package in.diantar.shop;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;

import in.diantar.shop.Product.showProduct;
import in.diantar.shop.viewKeranjang.cartController;

/**
 * Created by MasToro on 04/05/2017.
 */

public class ExpandListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<Group> groups;

    ImageLoader imageLoader = cartController.getmInstance().getmImageLoader();

    public ExpandListAdapter(Context context, ArrayList<Group> groups) {
        this.context = context;
        this.groups = groups;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        ArrayList<Child> chList = groups.get(groupPosition).getItems();
        return chList.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        Child child = (Child) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.child_item, null);
        }

        if (imageLoader == null)
            imageLoader = cartController.getmInstance().getmImageLoader();

        TextView tv = (TextView) convertView.findViewById(R.id.country_name);
        final TextView id = (TextView) convertView.findViewById(R.id.idkategori);
        NetworkImageView iv = (NetworkImageView) convertView
                .findViewById(R.id.flag);

        tv.setText(child.getName().toString());
        id.setText(child.getId().toString());
        id.setVisibility(View.INVISIBLE);
        iv.setImageUrl(String.valueOf(child.getImage()), imageLoader);


        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String idExKategori = id.getText().toString();
                if (context instanceof FragmentActivity) {
                    // We can get the fragment manager

                    android.support.v4.app.Fragment frg = new showProduct();
                    Bundle bundle = new Bundle();
                    bundle.putString("Category_ID", idExKategori);
                    frg.setArguments(bundle);

                    AppCompatActivity activity = (AppCompatActivity) v.getContext();
                    activity.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_frame, frg, null)
                            .addToBackStack(null)
                            .commit();
                }

            }
        });

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        ArrayList<Child> chList = groups.get(groupPosition).getItems();
        return chList.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        Group group = (Group) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) context
                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inf.inflate(R.layout.group_item, null);
        }
        TextView tv = (TextView) convertView.findViewById(R.id.group_name);
        tv.setText(group.getName());
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


}

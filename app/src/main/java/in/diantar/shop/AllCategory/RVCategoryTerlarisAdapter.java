package in.diantar.shop.AllCategory;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

import in.diantar.shop.CategoryTerlaris.ServerImageParseAdapter;
import in.diantar.shop.Product.showProduct;
import in.diantar.shop.R;

/**
 * Created by MasToro on 30/03/2017.
 */

public class RVCategoryTerlarisAdapter extends RecyclerView.Adapter<RVCategoryTerlarisAdapter.ViewHolder> {
    Context context;

    List<GetDataTerlarisAdapter> getDataTerlarisAdapters;

    ImageLoader imageLoader1;

    public RVCategoryTerlarisAdapter(List<GetDataTerlarisAdapter> getDataTerlarisAdapter, Context context) {

        super();
        this.getDataTerlarisAdapters = getDataTerlarisAdapter;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycleview_items_terlaris, parent, false);

        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder Viewholder, int position) {

        GetDataTerlarisAdapter getDataAdapter1 = getDataTerlarisAdapters.get(position);

        imageLoader1 = ServerImageParseAdapter.getInstance(context).getImageLoader();

        imageLoader1.get(getDataAdapter1.getProductImage(),
                ImageLoader.getImageListener(
                        Viewholder.networkImageView,//Server Image
                        R.mipmap.no_image,//Before loading server image the default showing image.
                        android.R.drawable.ic_menu_camera //Error image if requested image dose not found on server.
                )
        );

        Viewholder.networkImageView.setImageUrl(getDataAdapter1.getProductImage(), imageLoader1);
        Viewholder.namaProduk.setText(getDataAdapter1.getProductName());
        Viewholder.idProduk.setText(getDataAdapter1.getProductId());
        Viewholder.idProduk.setVisibility(View.INVISIBLE);

        Viewholder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String katID = Viewholder.idProduk.getText().toString();
                String katNM = Viewholder.namaProduk.getText().toString();

                // Toast.makeText(context, katID, Toast.LENGTH_SHORT).show();
                Fragment productFragment = showProduct.newInstance();
                Bundle bundle = new Bundle();
                bundle.putString("Category_ID", katID);
                bundle.putString("Category_NAME", katNM);
                productFragment.setArguments(bundle);
                Log.d("", String.valueOf(bundle));

                AppCompatActivity activity = (AppCompatActivity) view.getContext();
                activity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, productFragment)
                        .addToBackStack(null).commit();

            }
        });


    }

    @Override
    public int getItemCount() {

        return getDataTerlarisAdapters.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView namaProduk;
        public TextView idProduk;
        public NetworkImageView networkImageView;
        public CardView cardView;


        public ViewHolder(View itemView) {

            super(itemView);

            namaProduk = (TextView) itemView.findViewById(R.id.textView_item);
            idProduk = (TextView) itemView.findViewById(R.id.idProduk);
            cardView = (CardView) itemView.findViewById(R.id.cardview2);

            networkImageView = (NetworkImageView) itemView.findViewById(R.id.VollyNetworkImageView2);
            networkImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            //networkImageView.setRadius(15);


        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}

package in.diantar.shop.History_Order;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import in.diantar.shop.R;
import in.diantar.shop.viewKeranjang.ItemCart;
import in.diantar.shop.viewKeranjang.cartController;

/**
 * Created by MasToro on 03/04/2017.
 */

public class detilHistoryAdapter extends BaseAdapter {
    public static final String KEY_ORDER_ID = "order_id";
    public static final String KEY_DEVICE_ID = "device_id";


    private LayoutInflater inflater;
    private Activity activity;
    private List<ItemCart> itemCart;
    private Button tutup, orderlagi;

    private TextView idItem, price, qty, subtotText;


    ImageLoader imageLoader = cartController.getmInstance().getmImageLoader();
    String Rupiah = "Rp.";
    NumberFormat rupiahFormat;

    public detilHistoryAdapter(Activity activity, List<ItemCart> itemCart) {
        this.activity = activity;
        this.itemCart = itemCart;
    }


    @Override
    public int getCount() {
        return itemCart.size();
    }

    @Override
    public Object getItem(int position) {
        return itemCart.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void removeItem(int position) {
        itemCart.remove(position);
        notifyDataSetChanged();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.detil_history_layout, null);
        }
        if (imageLoader == null)
            imageLoader = cartController.getmInstance().getmImageLoader();

        NetworkImageView imageView = (NetworkImageView) convertView.findViewById(R.id.image_view);
        TextView title = (TextView) convertView.findViewById(R.id.nama_produk);

        imageView.setScaleX(1);

        idItem = (TextView) convertView.findViewById(R.id.idListItem);
        price = (TextView) convertView.findViewById(R.id.harga_produk);
        qty = (TextView) convertView.findViewById(R.id.qty_produk);

        subtotText = (TextView) convertView.findViewById(R.id.subTotal);

        tutup = (Button) convertView.findViewById(R.id.tutup);
        rupiahFormat = NumberFormat.getInstance(Locale.GERMANY);

        //getting data for row
        final ItemCart icart = itemCart.get(position);
        imageView.setImageUrl(icart.getImage(), imageLoader);


        // title.setText(itemCart.getName());
        title.setText(icart.getName());
        idItem.setText(icart.getId());
        qty.setText(icart.getQuantity());
        subtotText.setText(icart.getSubtotal());

        String rupiah = rupiahFormat.format(Double.parseDouble(icart.getPrice()));
        String sbtot = rupiahFormat.format(Double.parseDouble(icart.getSubtotal()));
        String Rp = Rupiah + " " + rupiah;
        String Stot = Rupiah + " " + sbtot;


        //Pake Logo Rp.
        price.setText(Rp);
        subtotText.setText(Stot);
        idItem.setVisibility(View.INVISIBLE);

        return convertView;
    }


}

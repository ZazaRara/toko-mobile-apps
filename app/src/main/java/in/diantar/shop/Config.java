package in.diantar.shop;


/**
 * Created by MasToro on 27/04/2017.
 */
public class Config {

    //JSON URL
    //   public static final String DATA_APR = "http://logvaksin-abar.com/android/apartement.php";
    public static final String DATA_APR = "http://bungakasih.id:8080/webservices/getdata.jsp?q=CLUSTER";

    public static final String CHECK_IN = "http://logvaksin-abar.com/android/checkin.php";
    public static final String SignInApartement_URL = "http://logvaksin-abar.com/android/signinapartement.php";
    public static final String UPDATE_CART_URL = "http://bungakasih.id:8080/webservices/simpan/index.jsp?q=UPDATEKERANJANG";
    public static final String GET_EXPAND_LIST_URL = "http://logvaksin-abar.com/android/expand.json";
    public static final String GET_USER_INFO = "http://bungakasih.id:8080/webservices/getdata.jsp?q=PENGGUNA&d=";
    //public static final String GET_EXPAND_LIST_URL = "http://bungakasih.id:8080/webservices/getdata.jsp?q=KATEGORI";
    public static final String GET_RIWAYAT_ORDER = "http://bungakasih.id:8080/webservices/getdata.jsp?q=RIWAYATORDER&d=";
    public static final String REMOVE_HISTORY = "http://bungakasih.id:8080/webservices/q=REMOVEHISTORY&d=";
    public static final String GET_CART_ITEM = "http://bungakasih.id:8080/webservices/getdata.jsp?q=ITEMKERANJANG&d=";
    public static final String GET_DETIL_RIWAYAT = "http://bungakasih.id:8080/webservices/getdata.jsp?q=RIWAYATORDERDETAIL&i=";
    public static final String GET_ALL_ITEM_CART = "http://bungakasih.id:8080/webservices/getdata.jsp?q=KERANJANG&d=";

    //"http://bungakasih.id:8080/webservices/simpan/?q=PENGGUNA&nama_apart="+nama_apartement+"&tower="+tower+"&lantai="+lantai+"&nomor="+nomor+"&email="+email+"&handphone="+nohp+"&device_id="+device_id

    public static final String KEY_DEVICE_ID = "device_id";

    //user info
    public static final String USER_APARTEMENT_ID = "id_apart";
    public static final String USER_MAIL = "email";
    public static final String USER_NAME = "nama";
    public static final String USER_PHONE_NUMBER = "handphone";
    public static final String USER_APARTEMENT_NAME = "nama_apart";
    public static final String USER_APARTEMENT_TOWER = "tower";
    public static final String USER_APARTEMENT_LANTAI = "lantai";
    public static final String USER_APARTEMENT_NOMOR = "nomor";

    //confirm
    public static final String KEY_CONFIRM_TANGGAL = "tgl";
    public static final String KEY_CONFIRM_JAM = "jam";
    public static final String KEY_CONFIRM_EMAIL = "email";

    //history order
    public static final String KEY_ORDER_ID = "id";
    public static final String KEY_NOMOR_ORDER = "nomor_order";
    public static final String KEY_TOTAL = "total";
    public static final String KEY_STATE = "state";
    public static final String KEY_TGL_ORDER = "tgl";

    //data pengguna apartemen
    public static final String KEY_NAME_APART = "apart";
    public static final String KEY_TOWER = "tower";
    public static final String KEY_LANTAI = "lantai";
    public static final String KEY_NOMOR = "nomor";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_NAMA_USER = "nama";
    public static final String KEY_HANDPHONE = "handphone";
    public static final String KEY_FCM_TOKEN = "fcm";

}


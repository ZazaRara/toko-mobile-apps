package in.diantar.shop;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class FireBaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fire_base);

        Intent myIntent = getIntent();
        String parameter = myIntent.getStringExtra("hasil");

        TextView hasil = (TextView) findViewById(R.id.hasilParam);
        hasil.setText(parameter);
    }
}

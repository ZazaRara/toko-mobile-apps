package in.diantar.shop.detilProduct;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import in.diantar.shop.CategoryTerlaris.ServerImageParseAdapter;
import in.diantar.shop.R;
import me.himanshusoni.quantityview.QuantityView;

/**
 * Created by MasToro on 30/03/2017.
 */

public class RVProductDetilAdapter extends RecyclerView.Adapter<RVProductDetilAdapter.ViewHolder> implements View.OnClickListener {
    Context context;

    List<GetProductDetilAdapter> getProductDetilAdapter;

    ImageLoader imageLoader1;

    String Rupiah = "Rp.";

    NumberFormat rupiahFormat;


    public RVProductDetilAdapter(List<GetProductDetilAdapter> getProductDetilAdapter, Context context) {
        super();
        this.getProductDetilAdapter = getProductDetilAdapter;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycleview_product_detil, parent, false);

        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder Viewholder, int position) {

        final GetProductDetilAdapter getProductDetilAdapter1 = getProductDetilAdapter.get(position);

        imageLoader1 = ServerImageParseAdapter.getInstance(context).getImageLoader();

        imageLoader1.get(getProductDetilAdapter1.getProductImage(),
                ImageLoader.getImageListener(
                        Viewholder.networkImageView,//Server Image
                        R.mipmap.image_blank,//Before loading server image the default showing image.
                        android.R.drawable.ic_dialog_alert //Error image if requested image dose not found on server.
                )
        );

        rupiahFormat = NumberFormat.getInstance(Locale.GERMANY);
        String rupiah = rupiahFormat.format(Double.parseDouble(getProductDetilAdapter1.getProductPrice()));
        String Result = Rupiah + " " + rupiah;

        Viewholder.networkImageView.setImageUrl(getProductDetilAdapter1.getProductImage(), imageLoader1);
        Viewholder.namaProduk.setText(getProductDetilAdapter1.getProductName());
        Viewholder.idProduk.setText(getProductDetilAdapter1.getProductId());
        Viewholder.hargaProduk.setText(Result);
        Viewholder.idProduk.setVisibility(View.INVISIBLE);
        Viewholder.Eqty.setMinQuantity(1);
        Viewholder.Eqty.setMaxQuantity(100);
        Viewholder.Eqty.setQuantity(1);

        Viewholder.cardViewDetil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(context, "Klik", Toast.LENGTH_SHORT).show();

            }
        });

        Viewholder.Eqty.setOnQuantityChangeListener(new QuantityView.OnQuantityChangeListener() {
            @Override
            public void onQuantityChanged(int oldQuantity, int newQuantity, boolean programmatically) {
                double price = Double.parseDouble(getProductDetilAdapter1.getProductPrice());
                double total = price * newQuantity;

                String hasil = rupiahFormat.format(total);
                Viewholder.hargaProduk.setText(Rupiah + " " + hasil);

            }

            @Override
            public void onLimitReached() {

            }
        });

    }


    @Override
    public int getItemCount() {
        return getProductDetilAdapter.size();
    }

    @Override
    public void onClick(View view) {

    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView namaProduk;
        public TextView idProduk;
        public TextView hargaProduk;
        public TextView descProduk;
        public NetworkImageView networkImageView;
        public CardView cardViewDetil;
        public QuantityView Eqty;


        public ViewHolder(View itemView) {

            super(itemView);

            namaProduk = (TextView) itemView.findViewById(R.id.nama);
            idProduk = (TextView) itemView.findViewById(R.id.Id);
            hargaProduk = (TextView) itemView.findViewById(R.id.harga);
            cardViewDetil = (CardView) itemView.findViewById(R.id.cardviewDetil);

            Eqty = (QuantityView) itemView.findViewById(R.id.qty);


            networkImageView = (NetworkImageView) itemView.findViewById(R.id.VollyNetworkImageView2);
            //networkImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        }
    }
}

package in.diantar.shop.CartFragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import in.diantar.shop.CheckOut;
import in.diantar.shop.Config;
import in.diantar.shop.R;
import in.diantar.shop.viewKeranjang.ItemCart;
import in.diantar.shop.viewKeranjang.cartAdapter;
import in.diantar.shop.viewKeranjang.cartController;


public class CartFragment extends Fragment {
    private ProgressDialog dialog;
    private List<ItemCart> itemKeranjang = new ArrayList<ItemCart>();
    private cartAdapter adapter;
    private TextView countCart, stotal, judul;
    String deviceID;
    RequestQueue requestQueue;
    LinearLayout subtotFooter, footer;
    Button shipPay, blonjosik, checkout;
    NumberFormat rupiahFormat;
    String Rupiah = "Rp.";
    private ImageButton backto;


    public static final String KEY_DEVICE_ID = "device_id";
    public static final String CHECKOUT_URL = "http://bungakasih.id:8080/webservices/simpan/index.jsp?q=BELI&device_id=";
    public static final String CEK_CART_URL = "http://bungakasih.id:8080/webservices/getdata.jsp?q=JUMLAHKERANJANG&device_id=";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_cart, container, false);//Inflate Layout
        deviceID = getArguments().getString("device_id");

        countCart = (TextView) getActivity().findViewById(R.id.countCart);

       /* shipPay = (Button)view.findViewById(R.id.shipPayment);
        shipPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ShipPayment.class); // the activity that holds the fragment
                Bundle bundle = ActivityOptions.makeCustomAnimation(getActivity(), R.anim.act_slideup, R.anim.act_slidedown).toBundle();
                startActivity(intent, bundle);

            }
        });*/

        checkout = (Button) view.findViewById(R.id.checkout);
        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String device_id = Settings.Secure.getString(getContext().getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                // checkOut(device_id);\

                Intent checkout = new Intent(getActivity(), CheckOut.class);
                startActivity(checkout);
            }
        });

        //deviceid = (TextView) view.findViewById(R.id.dvcId);
        //deviceid.setText(deviceID);

        return view;
    }


    private void checkOut(final String devId) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, CHECKOUT_URL + devId,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        response = response.trim();
                        if (response.equals("kosong")) {

                        } else {
                            Toasty.success(getContext(), "CheckOut Sukses", Toast.LENGTH_LONG).show();
                            jumlahItem(devId);
                        }
                       /* final String device_id = Settings.Secure.getString(getContext().getContentResolver(),
                                Settings.Secure.ANDROID_ID);
                         */
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(activity,error.toString(),Toast.LENGTH_LONG).show();
                        Toasty.error(getContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(KEY_DEVICE_ID, devId);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }


    private void jumlahItem(final String devId) {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(CEK_CART_URL + devId,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        //parsing json
                        if (response.length() == 0) {
                            //Toasty.info(getApplication(), "Keranjang Belanja Anda Kosong..."+response, Toast.LENGTH_SHORT).show();
                            countCart.setVisibility(View.INVISIBLE);
                        } else {
                            for (int x = 0; x < response.length(); x++) {
                                try {
                                    JSONObject obj = response.getJSONObject(x);
                                    //  Toast.makeText(activity, obj.getString("jml"), Toast.LENGTH_SHORT).show();
                                    countCart.setVisibility(View.VISIBLE);
                                    countCart.setText(obj.getString("jml"));
                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }
                            }

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Tidak ada koneksi Internet...";
                } else if (volleyError instanceof ServerError) {
                    message = "Server tidak ditemukan...";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Tidak ada koneksi Internet...";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing data Error...";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut...";
                }
                Toasty.error(getContext(), message, Toast.LENGTH_LONG).show();
            }
        });
        requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(jsonArrayRequest);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        judul = (TextView) getActivity().findViewById(R.id.titleToolbar);
        judul.setGravity(View.TEXT_ALIGNMENT_CENTER);
        judul.setText("Keranjang Belanja");

        backto = (ImageButton) getActivity().findViewById(R.id.backto);
        backto.setVisibility(View.VISIBLE);
        backto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backto.setVisibility(View.INVISIBLE);
                judul.setGravity(View.TEXT_ALIGNMENT_TEXT_START);
                judul.setText("diantar.in");
                getFragmentManager().popBackStack();
            }
        });

        blonjosik = (Button) getActivity().findViewById(R.id.blonjosik);
        blonjosik.setVisibility(View.VISIBLE);
        blonjosik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backto.setVisibility(View.INVISIBLE);
                judul.setGravity(View.TEXT_ALIGNMENT_TEXT_START);
                judul.setText("diantar.in");
                getFragmentManager().popBackStack();
            }
        });

        //list item di keranjang belanja
        final ListView lview = (ListView) view.findViewById(R.id.list_item);
        footer = (LinearLayout) view.findViewById(R.id.cartFooter);
        subtotFooter = (LinearLayout) view.findViewById(R.id.subTotLayout);
        stotal = (TextView) view.findViewById(R.id.sumtotal);


        footer.setVisibility(View.INVISIBLE);
        subtotFooter.setVisibility(View.INVISIBLE);
       /* cartProd = (TextView)getActivity().findViewById(R.id.cartProdName);
        cartProd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "click", Toast.LENGTH_SHORT).show();
            }
        });*/

        //adapter ListView Item Keranjang Belanja
        adapter = new cartAdapter(getActivity(), itemKeranjang, this);
        lview.setAdapter(adapter);

        dialog = new ProgressDialog(getActivity());
        dialog.setMessage("please wait...");
        dialog.show();

        //Creat volley request obj
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Config.GET_ALL_ITEM_CART + deviceID, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                hideDialog();
                //parsing json
                itemKeranjang.clear();
                if (response.length() == 0) {
                    footer.setVisibility(View.INVISIBLE);
                    subtotFooter.setVisibility(View.INVISIBLE);
                    RelativeLayout cartIsEmpty = (RelativeLayout) getActivity().findViewById(R.id.layoutEmptyCart);
                    lview.setVisibility(View.INVISIBLE);
                    cartIsEmpty.setVisibility(View.VISIBLE);
                    blonjosik = (Button) getActivity().findViewById(R.id.blonjosik);
                    blonjosik.setVisibility(View.VISIBLE);
                    blonjosik.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            backto.setVisibility(View.INVISIBLE);
                            judul.setGravity(View.TEXT_ALIGNMENT_TEXT_START);
                            judul.setText("diantar.in");
                            getFragmentManager().popBackStack();
                        }
                    });

                } else {
                    footer.setVisibility(View.VISIBLE);
                    subtotFooter.setVisibility(View.VISIBLE);
                    for (int x = 0; x < response.length(); x++) {
                        try {
                            JSONObject obj = response.getJSONObject(x);
                            ItemCart icart = new ItemCart();
                            icart.setId(obj.getString("product_id"));
                            icart.setName(obj.getString("nama"));
                            icart.setImage(obj.getString("image"));
                            icart.setPrice(obj.getString("product_price"));
                            icart.setSubtotal(obj.getString("subtotal"));
                            icart.setQuantity(obj.getString("quantity"));
                            //add to array

                            itemKeranjang.add(icart);

                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }
                    }
                    adapter.notifyDataSetChanged();

                    //hitung subtotal
                   /* double total=0;
                    int count = adapter.getCount();
                    for (int i=0;i<count;i++){
                        String dprice = itemKeranjang.get(i).getSubtotal();
                        total+=Double.parseDouble(dprice);

                    }
                    rupiahFormat = NumberFormat.getInstance(Locale.GERMANY);
                    String rp_total = rupiahFormat.format(total);
                    String hasil = Rupiah + " " + rp_total ;
                    stotal.setText(hasil);*/
                    hitungTotal();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        cartController.getmInstance().addToRequesQueue(jsonArrayRequest);


    }

    public void hitungTotal() {
        double total = 0;
        int count = adapter.getCount();
        for (int i = 0; i < count; i++) {
            String dprice = itemKeranjang.get(i).getSubtotal();
            total += Double.parseDouble(dprice);

        }
        rupiahFormat = NumberFormat.getInstance(Locale.GERMANY);
        String rp_total = rupiahFormat.format(total);
        String hasil = Rupiah + " " + rp_total;
        stotal.setText(hasil);

    }

    public void hideDialog() {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }
}

package in.diantar.shop.Product;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import in.diantar.shop.CategoryTerlaris.ServerImageParseAdapter;
import in.diantar.shop.R;
import in.diantar.shop.detilProduct.detilProduct;
import in.diantar.shop.viewKeranjang.ItemCart;
import in.diantar.shop.viewKeranjang.cartAdapter;
import in.diantar.shop.viewKeranjang.cartController;
import in.diantar.shop.viewKeranjang.orderProsesAdapter;


/**
 * Created by MasToro on 30/03/2017.
 */

public class RVProductAdapter extends RecyclerView.Adapter<RVProductAdapter.ViewHolder> {

    public static final String ADD_TO_CART_URL = "http://bungakasih.id:8080/webservices/simpan/index.jsp?q=KERANJANG";
    public static final String GET_CART_ITEM = "http://bungakasih.id:8080/webservices/getdata.jsp?q=LASTKERANJANG&d=";
    public static final String CEK_CART_URL = "http://bungakasih.id:8080/webservices/getdata.jsp?q=JUMLAHKERANJANG&device_id=";

    public static final String KEY_PRODUCT_ID = "product_id";
    public static final String KEY_NAME = "nama";
    public static final String KEY_PRODUCT_PRICE = "product_price";
    public static final String KEY_DEVICE_ID = "device_id";


    Context context;
    Activity activity;
    List<GetProductAdapter> getProductAdapter;
    ImageLoader imageLoader1;
    NumberFormat rupiahFormat;
    String Rupiah = "Rp.";
    private cartAdapter adapter;
    private orderProsesAdapter orderProses;
    private PopupWindow pwindo;
    private TextView xclose, countCart;
    private List<ItemCart> array = new ArrayList<ItemCart>();
    private ProgressDialog dialog;
    RequestQueue requestQueue;
    Button update, blonjo;


    public RVProductAdapter(List<GetProductAdapter> getProductAdapter, Context context) {
        super();
        this.getProductAdapter = getProductAdapter;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycleview_items_product, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder Viewholder, int position) {

        GetProductAdapter getProductAdapter1 = getProductAdapter.get(position);

        imageLoader1 = ServerImageParseAdapter.getInstance(context).getImageLoader();

        imageLoader1.get(getProductAdapter1.getProductImage(),
                ImageLoader.getImageListener(
                        Viewholder.networkImageView,//Server Image
                        R.mipmap.image_blank,//Before loading server image the default showing image.
                        android.R.drawable.ic_dialog_alert //Error image if requested image dose not found on server.
                )
        );

        rupiahFormat = NumberFormat.getInstance(Locale.GERMANY);
        String rupiah = rupiahFormat.format(Double.parseDouble(getProductAdapter1.getProductPrice()));
        String Result = Rupiah + " " + rupiah;

        Viewholder.networkImageView.setImageUrl(getProductAdapter1.getProductImage(), imageLoader1);


        Viewholder.namaProduk.setText(getProductAdapter1.getProductName());
        Viewholder.hargaProduk.setText(Result);
        Viewholder.hargaProdukInv.setText(getProductAdapter1.getProductPrice());
        Viewholder.idProduk.setText(getProductAdapter1.getProductId());
        Viewholder.idProduk.setVisibility(View.INVISIBLE);

        Viewholder.cardViewProduk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String prodID = Viewholder.idProduk.getText().toString();
                String hrgProd = Viewholder.hargaProdukInv.getText().toString();
                String namaProd = Viewholder.namaProduk.getText().toString();

                // Toast.makeText(context, prodID, Toast.LENGTH_SHORT).show();

                Fragment detilproduk = detilProduct.newInstance();
                Bundle bundle = new Bundle();
                bundle.putString("Product_ID", prodID);
                bundle.putString("Product_PRICE", hrgProd);
                bundle.putString("Product_NAME", namaProd);

                detilproduk.setArguments(bundle);
                Log.d("", String.valueOf(bundle));

                AppCompatActivity activity = (AppCompatActivity) view.getContext();

                activity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, detilproduk)
                        .addToBackStack(null).commit();

            }
        });

        Viewholder.TbBeli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String productId = Viewholder.idProduk.getText().toString().trim();
                final String product_price = Viewholder.hargaProdukInv.getText().toString().trim();
                final String nama = Viewholder.namaProduk.getText().toString().trim();
                final String device_id = Settings.Secure.getString(context.getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                //  Toast.makeText(context, product_price , Toast.LENGTH_SHORT).show();

                //  Toast.makeText(context, device_id, Toast.LENGTH_SHORT).show();
                addToCart(productId, product_price, device_id, view, nama);
            }
        });

    }

    private void showOrderPopup(View v, String device_id) {
        try {

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
            final View layout = inflater.inflate(R.layout.frame_popup,
                    (ViewGroup) v.findViewById(R.id.popup_container));


            pwindo = new PopupWindow(context);
            pwindo.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
            layout.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            pwindo.setWidth(layout.getMeasuredWidth());
            pwindo.setFocusable(true);
            pwindo.setOutsideTouchable(true);
            pwindo.setBackgroundDrawable(new ColorDrawable(Color.alpha(2)));
            pwindo.setContentView(layout);
            pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);


            //list item di keranjang belanja
            final ListView lview = (ListView) layout.findViewById(R.id.list_item);
            // idItem = (TextView)layout.findViewById(R.id.idListItem);


            update = (Button) layout.findViewById(R.id.updateOrder);
            update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TextView harga = (TextView) layout.findViewById(R.id.priceProduct);
                    Toast.makeText(context, harga.getText().toString(), Toast.LENGTH_SHORT).show();


                }
            });

            blonjo = (Button) layout.findViewById(R.id.belanjaLagi);
            blonjo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pwindo.dismiss();
                }
            });

            //adapter ListView Item Keranjang Belanja
            orderProses = new orderProsesAdapter((Activity) context, array);
            lview.setAdapter(orderProses);


            dialog = new ProgressDialog(context);
            dialog.setMessage("mohon tunggu...");
            dialog.show();

            //Creat volley request obj
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(GET_CART_ITEM + device_id,
                    new Response.Listener<JSONArray>() {

                        @Override
                        public void onResponse(JSONArray response) {
                            hideDialog();
                            //parsing json
                            array.clear();
                            if (response.length() == 0) {
                                Toasty.info(context, "Keranjang Belanja Anda Kosong...", Toast.LENGTH_SHORT).show();
                            } else {
                                for (int x = 0; x < response.length(); x++) {
                                    try {
                                        JSONObject obj = response.getJSONObject(x);

                                        ItemCart icart = new ItemCart();
                                        icart.setId(obj.getString("product_id"));
                                        icart.setName(obj.getString("nama"));
                                        icart.setImage(obj.getString("image"));
                                        icart.setPrice(obj.getString("product_price"));
                                        icart.setSubtotal(obj.getString("subtotal"));
                                        icart.setQuantity(obj.getString("quantity"));
                                        //add to array
                                        array.add(icart);

                                    } catch (JSONException ex) {
                                        ex.printStackTrace();
                                    }
                                }
                                orderProses.notifyDataSetChanged();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError volleyError) {
                    String message = null;
                    if (volleyError instanceof NetworkError) {
                        message = "Tidak ada koneksi Internet...";
                    } else if (volleyError instanceof ServerError) {
                        message = "Server tidak ditemukan...";
                    } else if (volleyError instanceof AuthFailureError) {
                        message = "Tidak ada koneksi Internet...";
                    } else if (volleyError instanceof ParseError) {
                        message = "Parsing data Error...";
                    } else if (volleyError instanceof TimeoutError) {
                        message = "Connection TimeOut...";
                    }
                    Toasty.error(context, message, Toast.LENGTH_LONG).show();
                    hideDialog();
                }
            });
            cartController.getmInstance().addToRequesQueue(jsonArrayRequest);
            //close popup pojok kanan atas
            xclose = (TextView) layout.findViewById(R.id.close_cart);
            xclose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    lview.setAdapter(null);
                    pwindo.dismiss();

                }

            });

            countCart = (TextView) ((Activity) context).findViewById(R.id.countCart);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void hideDialog() {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }

    private void jumlahItem(final String devId) {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(CEK_CART_URL + devId,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        //parsing json
                        if (response.length() == 0) {
                            //Toasty.info(getApplication(), "Keranjang Belanja Anda Kosong..."+response, Toast.LENGTH_SHORT).show();
                            countCart.setVisibility(View.INVISIBLE);
                        } else {
                            for (int x = 0; x < response.length(); x++) {
                                try {
                                    JSONObject obj = response.getJSONObject(x);
                                    // Toast.makeText(MainActivity.this, obj.getString("jml"), Toast.LENGTH_SHORT).show();
                                    countCart.setVisibility(View.VISIBLE);
                                    countCart.setText(obj.getString("jml"));
                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }
                            }

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Tidak ada koneksi Internet...";
                } else if (volleyError instanceof ServerError) {
                    message = "Server tidak ditemukan...";
                } else if (volleyError instanceof AuthFailureError) {
                    message = "Tidak ada koneksi Internet...";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing data Error...";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut...";
                }
                Toasty.error(context, message, Toast.LENGTH_LONG).show();
            }
        });
        requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(jsonArrayRequest);
    }

    public void addToCart(final String productId, final String product_price, final String device_id, final View view, final String nama) {
        final ProgressDialog loading = ProgressDialog.show(context, "Please Wait...", "Memasukan ke Keranjang Belanja...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ADD_TO_CART_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loading.dismiss();
                        // Toast.makeText(context,response,Toast.LENGTH_LONG).show();
                        //  response=response.trim();
                        //  Toasty.success(context,response, Toast.LENGTH_LONG).show();
                        jumlahItem(device_id);
                        showOrderPopup(view, device_id);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(context,error.toString(),Toast.LENGTH_LONG).show();
                        Toasty.error(context, error.toString(), Toast.LENGTH_LONG).show();
                        hideDialog();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(KEY_PRODUCT_ID, productId);
                params.put(KEY_PRODUCT_PRICE, product_price);
                params.put(KEY_DEVICE_ID, device_id);
                params.put(KEY_NAME, nama);

                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    @Override
    public int getItemCount() {

        return getProductAdapter.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView namaProduk;
        public TextView idProduk;
        public TextView hargaProduk;
        public TextView hargaProdukInv;
        public NetworkImageView networkImageView;
        public CardView cardViewProduk;
        public Button TbBeli;


        public ViewHolder(View itemView) {

            super(itemView);

            namaProduk = (TextView) itemView.findViewById(R.id.namaProduk);
            idProduk = (TextView) itemView.findViewById(R.id.IdProduct);
            hargaProduk = (TextView) itemView.findViewById(R.id.price);
            hargaProdukInv = (TextView) itemView.findViewById(R.id.priceInv);
            cardViewProduk = (CardView) itemView.findViewById(R.id.cardviewProduct);
            TbBeli = (Button) itemView.findViewById(R.id.addToCart);


            networkImageView = (NetworkImageView) itemView.findViewById(R.id.VollyNetworkImageView1);
            //networkImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}

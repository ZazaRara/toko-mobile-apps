package in.diantar.shop;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import es.dmoral.toasty.Toasty;

import static java.lang.Integer.parseInt;

public class PilihApart extends AppCompatActivity {

    //Declaring an Spinner
    private MaterialSpinner spinner;
    // private Spinner pilihan;

    //An ArrayList for Spinner Items
    private ArrayList<String> apartements;

    RequestQueue requestQueue;
    ProgressDialog dialog;
    Button submitBtn;
    EditText tower, lantai, nomor, email, hp, nama;
    TextView deviceID;


    boolean doubleBackToExitPressedOnce = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilih_apart);

        final String device_id = Settings.Secure.getString(getApplication().getContentResolver(),
                Settings.Secure.ANDROID_ID);

        //Initializing the ArrayList
        apartements = new ArrayList<String>();

        //Initializing Spinner
        spinner = (MaterialSpinner) findViewById(R.id.spinner);
        // pilihan = (Spinner)findViewById(R.id.pilapart);

        //Initializing EditText
        tower = (EditText) findViewById(R.id.tower);
        lantai = (EditText) findViewById(R.id.lantai);
        nomor = (EditText) findViewById(R.id.nomor);
        email = (EditText) findViewById(R.id.email);
        nama = (EditText) findViewById(R.id.nama);
        hp = (EditText) findViewById(R.id.nohp);

        submitBtn = (Button) findViewById(R.id.submit);
        deviceID = (TextView) findViewById(R.id.device);


        deviceID.setText(device_id);

        //Adding an Item Selected Listener to our Spinner
        //As we have implemented the class Spinner.OnItemSelectedListener to this class iteself we are passing this to setOnItemSelectedListener

        email.setText(getEmailUser(getApplicationContext()));
        hp.setText("+62");
        hp.setFocusable(true);
        getApartement();


        //  spinner.setItems(ANDROID_VERSIONS);
        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                tower.setText("");
                lantai.setText("");
                nomor.setText("");
                //Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
            }
        });
        spinner.setOnNothingSelectedListener(new MaterialSpinner.OnNothingSelectedListener() {

            @Override
            public void onNothingSelected(MaterialSpinner spinner) {
                Snackbar.make(spinner, "Nothing selected", Snackbar.LENGTH_LONG).show();
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String emailUser = email.getText().toString().trim();
                //    validateEmail(emailUser);
                if ((tower.getText().length() != 0) && (lantai.getText().length() != 0) && (nomor.getText().length() != 0)) {
                    //Snackbar.make(view, "Lanjut", Snackbar.LENGTH_LONG).show();
                    String nama_ap = spinner.getText().toString();
                    String tower_ap = tower.getText().toString();
                    String lantai_ap = lantai.getText().toString();
                    String nomor_ap = nomor.getText().toString();
                    String email_ap = email.getText().toString().trim();
                    String nama_us = nama.getText().toString();
                    String nohp_ap = hp.getText().toString();
                    String fcm_token = FirebaseInstanceId.getInstance().getToken();

                    SignInApartement(device_id, nama_ap, tower_ap, lantai_ap, nomor_ap, email_ap, nama_us, nohp_ap, fcm_token);

                } else {
                    Snackbar.make(view, "Mohon isikan informasi Apartement anda dengan lengkap", Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }


    public void validateEmail(final View view, final String emailTxt) {

        final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        email.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                if (email.equals(emailPattern) && s.length() > 0) {
                    // Toast.makeText(getApplicationContext(),"valid email address",Toast.LENGTH_SHORT).show();
                    Snackbar.make(view, "email valid", Snackbar.LENGTH_SHORT).show();
                } else {
                    //Toast.makeText(getApplicationContext(),"Invalid email address",Toast.LENGTH_SHORT).show();
                    Snackbar.make(view, "Invalid email address", Snackbar.LENGTH_SHORT).show();
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // other stuffs
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // other stuffs
            }
        });
    }


    private String getEmailID(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        Account account = getAccount(accountManager);
        if (account == null) {
            return null;
        } else {
            return account.name;
        }
    }


    private String getEmailUser(Context context) {
        String possibleEmail = "";
        final Account[] accounts = AccountManager.get(context).getAccounts();
        //Log.e("accounts","->"+accounts.length);
        for (Account account : accounts) {
            if (Patterns.EMAIL_ADDRESS.matcher(account.name).matches()) {
                possibleEmail = account.name;
            }
        }
        return possibleEmail;
    }

    private static Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        }
        return account;
    }

    private void SignInApartement(final String device_id, final String nama_apartement, final String tower, final String lantai, final String nomor, final String email, final String nama, final String nohp, final String fcm) {

        final ProgressDialog loading = ProgressDialog.show(this, "", "please wait..", false, false);

        //StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://bungakasih.id:8080/webservices/simpan/?q=PENGGUNA",
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://bungakasih.id:8080/webservices/simpan/pengguna.jsp?q=PENGGUNA",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loading.dismiss();
                        // Toast.makeText(context,response,Toast.LENGTH_LONG).show();
                        //   response=response.trim();
                        // Toasty.success(getApplicationContext(),response, Toast.LENGTH_LONG).show();
                        finish();
                        Intent main = new Intent(PilihApart.this, MainActivity.class);
                        startActivity(main);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Toast.makeText(context,error.toString(),Toast.LENGTH_LONG).show();
                        Toasty.error(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(Config.KEY_DEVICE_ID, device_id);
                params.put(Config.KEY_NAME_APART, nama_apartement);
                params.put(Config.KEY_TOWER, tower);
                params.put(Config.KEY_LANTAI, lantai);
                params.put(Config.KEY_NOMOR, nomor);
                params.put(Config.KEY_EMAIL, email);
                params.put(Config.KEY_NAMA_USER, nama);
                params.put(Config.KEY_HANDPHONE, nohp);
                params.put(Config.KEY_FCM_TOKEN, fcm);
                return params;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void getApartement() {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Config.DATA_APR,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        //parsing json
                        hideDialog();
                        if (response.length() == 0) {
                            Toasty.info(getApplication(), "Kosong..." + response, Toast.LENGTH_SHORT).show();

                        } else {
                            for (int x = 0; x < response.length(); x++) {
                                try {
                                    JSONObject obj = response.getJSONObject(x);
                                    String nama = obj.getString("nama");
                                    int id = parseInt(obj.getString("id"));
                                    // Toast.makeText(getApplicationContext(), nama, Toast.LENGTH_SHORT).show();
                                    apartements.add(nama);
                                    // apartements.add(nama);
                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }
                            }
                            // spinner.setAdapter(new ArrayAdapter<String>(PilihApart.this, android.R.layout.simple_list_item_checked, apartements));
                            // ArrayAdapter<String> apart = new ArrayAdapter<String>(PilihApart.this, R.layout.spinner_item, apartements);
                            spinner.setItems(apartements);
                            // apart.setDropDownViewResource(R.layout.spinner_dropdown_item);
                            // pilihan.setAdapter(apart);

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                String message = null;
                if (volleyError instanceof NetworkError) {
                    message = "Tidak ada koneksi Internet...";
                } else if (volleyError instanceof ServerError) {
                    message = "Server tidak ditemukan...";
                } else if (volleyError instanceof ParseError) {
                    message = "Parsing data Error...";
                } else if (volleyError instanceof TimeoutError) {
                    message = "Connection TimeOut...";
                }
                Toasty.error(getApplicationContext(), message, Toast.LENGTH_LONG).show();


            }
        });
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
    }

    public void hideDialog() {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }

    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toasty.info(this, "Tekan sekali lagi untuk keluar", Toast.LENGTH_SHORT).show();


        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2500);

    }

}
